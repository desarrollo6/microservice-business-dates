<?php

return [
    'US'=>[
        ['date'=>'2018-12-25', 'reason'=>'Christmas Day'],
        ['date'=>'2018-12-31', 'reason'=>'New Year’s Day'],
        ['date'=>'2019-01-01', 'reason'=>'New Year’s Day'],
        ['date'=>'2019-01-21', 'reason'=>'Martin Luther King, Jr., Day'],
        ['date'=>'2019-02-18', 'reason'=>'Presidents Day'],
        ['date'=>'2019-05-27', 'reason'=>'Memorial Day'],
        ['date'=>'2019-07-04', 'reason'=>'Independence Day'],
        ['date'=>'2019-09-02', 'reason'=>'Labor Day'],
        ['date'=>'2019-10-14', 'reason'=>'Columbus Day'],
        ['date'=>'2019-11-11', 'reason'=>'Veterans Day'],
        ['date'=>'2019-11-28', 'reason'=>'Thanksgiving Day'],
        ['date'=>'2019-12-25', 'reason'=>'Christmas Day'],
        ['date'=>'2019-12-31', 'reason'=>'New Year’s Day'],

        ['date'=>'2020-01-01', 'reason'=>'New Year’s Day'],
        ['date'=>'2020-01-20', 'reason'=>'Martin Luther King, Jr., Day'],
        ['date'=>'2020-02-17', 'reason'=>'Presidents Day'],
        ['date'=>'2020-05-25', 'reason'=>'Memorial Day'],
        ['date'=>'2020-07-03', 'reason'=>'Independence Day'],
        ['date'=>'2020-09-07', 'reason'=>'Labor Day'],
        ['date'=>'2020-10-12', 'reason'=>'Columbus Day'],
        ['date'=>'2020-11-11', 'reason'=>'Veterans Day'],
        ['date'=>'2020-11-26', 'reason'=>'Thanksgiving Day'],
        ['date'=>'2020-12-25', 'reason'=>'Christmas Day'],
        ['date'=>'2020-12-31', 'reason'=>'New Year’s Day'],
    ]
];