<?php
namespace Tests\Traits\Unit;

trait  ApiTestTrait{

    /**
     * @var string
     */
    protected $api_base = '/api/v1/businessDates/';

    protected $testing_methods = ['GET','POST'];

}