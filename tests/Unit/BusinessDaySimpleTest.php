<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\BusinessDay;

class BusinessDaySimpleTest extends TestCase
{
    /**
     * Testing holidays not found.
     * @return void
     */
    public function testNotExist()
    {
        $bd = new BusinessDay();
        $bd->set_locale("CODE_ISO_NOT_FOUND");
        $this->assertCount(0,$bd->holidays);
    }

    /**
     * Testing valid holidays
     */
    public function testExist()
    {
        $bd = new BusinessDay();
        $bd->set_locale("us");
        $this->assertGreaterThan(0,$bd->holidays);
    }
}
