<?php

namespace Tests\Unit\v1;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\Unit\ApiTestTrait;

class ApiIsBusinessDayTest extends TestCase
{
    use ApiTestTrait;

    protected $end_point            = 'isBusinessDay';
    protected $payload_out_true     = ['ok' => true];
    protected $payload_out_false    = ['ok' => false];


    /**
     * Testing empty request
     */
    public function testEmpty()
    {
        foreach($this->testing_methods as $method){

            $this
                ->json($method, $this->api_base.$this->end_point, [])
                ->assertStatus(400) //Wrong request - Missing parameters
            ;

        }

    }

    /**
     * Testing extra parameters
     */
    public function testExtra()
    {

        foreach($this->testing_methods as $method){

            $payload_wrong = [
                'initialDate'   => '2018-12-12T10:10:10Z',
                'delay'         => 3 //Extra parameter
            ];

            $this
                ->json($method, $this->api_base.$this->end_point, $payload_wrong)
                ->assertStatus(400) //Wrong request - Extra parameters
            ;

        }

    }

    /**
     * Testing specific date
     */
    public function testTrue20181212()
    {
        foreach($this->testing_methods as $method){

            $payload_right = [
                'initialDate'   => '2018-12-12T10:10:10Z',
            ];

            $this
                ->json($method, $this->api_base.$this->end_point, $payload_right)
                ->assertStatus(200)
                ->assertJson($this->payload_out_true)
            ;

        }

    }

    /**
     * Testing specific weekend
     */
    public function testWeekend20190105()
    {
        foreach($this->testing_methods as $method){

            $payload_right = [
                'initialDate'   => '2019-01-05T10:10:10Z',
            ];

            $this
                ->json($method, $this->api_base.$this->end_point, $payload_right)
                ->assertStatus(200)
                ->assertJson($this->payload_out_false)
            ;

        }

    }

    /**
     * Testing specific presidents day
     */
    public function testPresidentsDay20190218()
    {
        foreach($this->testing_methods as $method){

            $payload_right = [
                'initialDate'   => '2019-02-18T10:10:10Z',
            ];

            $this
                ->json($method, $this->api_base.$this->end_point, $payload_right)
                ->assertStatus(200)
                ->assertJson($this->payload_out_false)
            ;

        }

    }


}
