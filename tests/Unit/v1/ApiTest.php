<?php

namespace Tests\Unit\v1;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\Unit\ApiTestTrait;

class ApiTest extends TestCase
{
    use ApiTestTrait;


    /**
     * Testing endpoints list
     *
     * @return void
     */
    public function testList()
    {
        $json_response =[
            'isBusinessDay',
            'getBusinessDateWithDelay'
        ];
        $response = $this->get($this->api_base);

        $response
            ->assertStatus(200)
            ->assertJson($json_response)
        ;
    }

}
