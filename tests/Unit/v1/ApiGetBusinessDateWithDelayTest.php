<?php

namespace Tests\Unit\v1;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\Unit\ApiTestTrait;

class ApiGetBusinessDateWithDelayTest extends TestCase
{
    use ApiTestTrait;
    protected $end_point = 'getBusinessDateWithDelay';


    /**
     * Testing christmas date from api
     */
    public function testJanuary()
    {
        foreach($this->testing_methods as $method){

            $payload_right = [
                'initialDate'   => '2018-12-25T10:10:10Z',
                'delay'         =>  20
            ];

            $payload_out=[
                'ok'            => false,
                'initialQuery'  => [
                    'initialDate'   => "2018-12-25T10:10:10Z",
                    'delay'         => 20
                ],
                'results'   => [
                    'businessDate'  => '2019-01-25',
                    'totalDays'     => 31,
                    'holidayDays'   => 4,
                    'weekendDays'   => 8
                ]
            ]
            ;

            $this
                ->json($method, $this->api_base.$this->end_point, $payload_right)
                ->assertStatus(200)
                ->assertJson($payload_out)
            ;

        }

    }


    /**
     * Testing november date from api
     */
    public function testNovember()
    {
        foreach($this->testing_methods as $method){

            $payload_right = [
                'initialDate'   => '2019-11-10T10:10:10Z',
                'delay'         =>  3
            ];

            $payload_out=[
                'ok'            => false,
                'initialQuery'  => [
                    'initialDate'   => "2019-11-10T10:10:10Z",
                    'delay'         => 3
                ],
                'results'   => [
                    'businessDate'  => '2019-11-14',
                    'totalDays'     => 4,
                    'holidayDays'   => 1,
                    'weekendDays'   => 1
                ]
            ]
            ;

            $this
                ->json($method, $this->api_base.$this->end_point, $payload_right)
                ->assertStatus(200)
                ->assertJson($payload_out)
            ;

        }

    }
}
