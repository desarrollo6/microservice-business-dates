<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\BusinessDay;
use \DateTime;

class BusinessDayDelayedTest extends TestCase
{
    /**
     * Testing from christmas date.
     *
     * @return void
     */
    public function testChristmas()
    {
        $bd = new BusinessDay();
        $bd->set_locale('us');
        $calculated = $bd->calculate_business_date(new DateTime('2018-12-25'),4);
        $expected=[
            'businessDate'  =>  '2019-01-02',
            'totalDays'     =>  8,
            'holidayDays'   =>  3,
            'weekendDays'   =>  2,
        ];
        $this->assertSame($expected,$calculated);
    }

    /**
     *
     * Testing from Veterans Day
     * @return void
     */
    public function testVeteransDay()
    {
        $bd = new BusinessDay();
        $bd->set_locale('us');
        $calculated = $bd->calculate_business_date(new DateTime('2019-11-10'),4);
        $expected=[
            'businessDate'  =>  '2019-11-15',
            'totalDays'     =>  5,
            'holidayDays'   =>  1,
            'weekendDays'   =>  1,
        ];

        $this->assertSame($expected,$calculated);
    }
}
