<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(['prefix'=>'v1/businessDates'], function() {

    Route::get('/', 'V1\ApiController@index')->name('v1.index');
    Route::get('isBusinessDay', 'V1\ApiController@isBusinessDay')->name('v1.isBusinessDay');
    Route::get('getBusinessDateWithDelay', 'V1\ApiController@getBusinessDateWithDelay')->name('v1.getBusinessDateWithDelay');

    Route::post('isBusinessDay', 'V1\ApiController@isBusinessDay')->name('v1.isBusinessDay');
    Route::post('getBusinessDateWithDelay', 'V1\ApiController@getBusinessDateWithDelay')->name('v1.getBusinessDateWithDelay');

});
