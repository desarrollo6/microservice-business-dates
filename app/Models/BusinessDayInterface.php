<?php
declare(strict_types=1);

namespace App\Models;

use DateTime;


/**
 * BusinessDay Interface
 * @package App\Models
 */
interface BusinessDayInterface
{
    /**
     *
     * Configure locale variable in order to get holidays,
     * there is a list of holiday days in config/holiday_days.php file.
     *
     * @param string $locale
     * @return bool
     */
    function set_locale(string $locale):bool;

    /**
     *
     * Checking if is a business day
     *
     * @param DateTime $dateTime
     * @return bool
     */
    function is_business_day(DateTime $dateTime):bool;

    /**
     *
     * Calculate business day with delay parameter
     *
     * @param DateTime $start
     * @param int $delay
     * @return array
     */
    function calculate_business_date(DateTime $start,int $delay):array;

}