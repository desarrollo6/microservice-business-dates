<?php


namespace App\Models;
use \DateTime;

/**
 * BusinessDay Implementation
 * @package App\Models
 */
final class BusinessDay implements BusinessDayInterface
{

    public $holidays=[];


    /**
     * @inheritDoc
     */
    function set_locale(string $locale='us'): bool
    {
        $locale=strtoupper(trim($locale));
        $_tmp = config('holiday_days.'.$locale);
        if(is_null($_tmp)) return false;
        $this->holidays = array_column($_tmp, 'date');
        return true;
    }


    /**
     * @inheritDoc
     */
    function is_business_day(DateTime $dateTime): bool
    {
        $is = true;
        if($dateTime->format('N') >= 6) $is = false;
        $key = array_search($dateTime->format('Y-m-d'), $this->holidays);
        if($key!==false) $is=false;

        return $is;
    }

    /**
     * @inheritDoc
     */
    function calculate_business_date(DateTime $start, int $delay): array
    {
        //echo "start {$start->format('Y-m-d')} delay {$delay}\n";
        $count=0;
        $totalDays=0;
        $weekendDays=0;
        $holidayDays=0;
        $currentDate = new DateTime($start->format('Y-m-d'));
        while($count<=$delay){
            //echo "\ndate {$currentDate->format('Y-m-d')} count {$count}\n";
            if($this->is_business_day($currentDate)){
                $count++;
                //echo "is_business_day\n";
                if($count==$delay) break;
            }else{
                if($currentDate->format('N') >= 6){
                    $weekendDays++;
                    //echo "is_weekend_day\n";
                }else{
                    $holidayDays++;
                    //echo "is_holi_day\n";
                }
            }
            $totalDays++;
            $currentDate->modify('+1 day');
            //echo "\n";
        }



        return [
            'businessDate'  =>  $currentDate->format('Y-m-d'),
            'totalDays'     =>  $totalDays,
            'holidayDays'   =>  $holidayDays,
            'weekendDays'   =>  $weekendDays,
        ];
    }


}