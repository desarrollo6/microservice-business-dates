<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BusinessDay;
use \DateTime;

class ApiController extends Controller
{
    /**
     * Shows a list of available endpoints
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        return response()->json([
            'isBusinessDay',
            'getBusinessDateWithDelay'
        ],200);
    }

    /**
     *
     * Just checks if a date is a business day or not
     *
     * @param Request $request
     * @param BusinessDay $bd
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function isBusinessDay(Request $request,BusinessDay $bd){

        $initialDate = $request->get('initialDate');

        if(is_null($initialDate))
            return response()->json(['message'=>'Missing initialDate parameter'],400);


        if((int)$request->get('delay')>0)
            return response()->json(['message'=>'Extra delay parameter'],400);


        if(!$bd->set_locale($request->get('locale','US')))
            $bd->set_locale('us');


        if($bd->is_business_day(new DateTime($initialDate)))
            return response()->json(['ok'=>true],200);


        /*IS NOT A BUSINESS DAY*/
        return response()->json(['ok'=>false],200);
    }

    /**
     *
     * Checks if a date is a business day with delay parameter
     *
     * @param Request $request
     * @param BusinessDay $bd
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getBusinessDateWithDelay(Request $request,BusinessDay $bd){
        $initialDate = $request->get('initialDate');
        $delay = $request->get('delay');

        if(is_null($initialDate))
            return response()->json(['message'=>'Missing initialDate parameter'],400);

        if(is_null($delay))
            return response()->json(['message'=>'Missing delay parameter'],400);

        if(!$bd->set_locale($request->get('locale','US')))
            $bd->set_locale('us');

        $results = $bd->calculate_business_date(new DateTime($initialDate),$delay);



        $ok = $bd->is_business_day(new DateTime($initialDate));
        $response =[
            'ok'            =>  $ok,
            'initialQuery'  =>  $request->all(),
            'results'       =>  $results ,
        ];

        return response()->json($response,200);
    }
}
